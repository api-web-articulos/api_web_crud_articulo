﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiRestYuli.Models
{
    public class ArtViewModels
    {
        public int idarticulo { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string CodigoBarras { get; set; }
        public string CodigoFabricante { get; set; }
    }
}