﻿using ApiRestYuli.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

/***********************************************************************************************************************
* Nombre del aplicativo              : CRUD de Articulos
* Archivo                            : ArticuloController.cs
* Proposito                          : Funcionalidad para Crear, Modificar, Eliminar y Consultar. 
*
* Fecha de Creacion/Modificacion     : Miercoles, 6 de Noviembre del 2019
* Responsable Ultima Modificación    : Yuliana Mata Corchado
*
/**********************************************************************************************************************/

namespace ApiRestYuli.Controllers
{
    public class ArticuloController : ApiController
    {
        /***********************************************************************************************************************
        * Propósito                          : Realizar peticion POST (Para Agregar un nuevo articulo a la base de datos). 
        *
        * Parámetros In                      : Nombre del articulo, Descriocion,Codigo de barras, codigo del fabricante
        * Parámetros Out                     : Mensaje de confirmacion
        *
        * Fecha de Creacion/Modificacion     : Miercoles, 6 de Noviembre del 2019
        * Responsable Ultima Modificación    : Yuliana Mata Corchado
        *
        /**********************************************************************************************************************/
        [HttpPost]
        public IHttpActionResult agregarArticulo(Models.ArtViewModels articulomodelo)
        {
            if (!ModelState.IsValid)
                return BadRequest("Dato invalido");

            using (Models.articulosEntities db = new Models.articulosEntities())
            {
                db.articulo.Add(new articulo()
                {
                    nombre = articulomodelo.Nombre,
                    descripcion = articulomodelo.Descripcion,
                    codigoBarras = articulomodelo.CodigoBarras,
                    codigoFabricante = articulomodelo.CodigoFabricante
                });

                db.SaveChanges();
            }
            return Ok("Dato guardado.");
        }



        /***********************************************************************************************************************
        * Propósito                          : Realizar peticion GET (Consultar todos los productos que esatan 
        *                                      registrados en la base de datos).
        *
        * Parámetros In                      : Ninguno.
        * Parámetros Out                     : Json con los articulos registrados.
        *
        * Fecha de Creacion/Modificacion     : Miercoles, 6 de Noviembre del 2019
        * Responsable Ultima Modificación    : Yuliana Mata Corchado
        *
        /**********************************************************************************************************************/
        [HttpGet]
        public IHttpActionResult consultaArticulos()
        {
            List<ViewModels.ArticuloViewModel> lst = new List<ViewModels.ArticuloViewModel>();
            using (Models.articulosEntities db = new Models.articulosEntities())
            {
                lst = (from d in db.articulo
                       select new ViewModels.ArticuloViewModel
                       {
                           idarticulo = d.idarticulo,
                           Nombre = d.nombre,
                           CodigoBarras = d.codigoBarras,
                           CodigoFabricante = d.codigoFabricante
                       }).ToList();
            }
            return Ok(lst);
        }


        /***********************************************************************************************************************
        * Propósito                          : Realizar peticion DELETE (Para eliminar un articulo de la base de datos).
        *
        * Parámetros In                      : ID del articulo.
        * Parámetros Out                     : Mensaje de confirmacion.
        *
        * Fecha de Creacion/Modificacion     : Miercoles, 6 de Noviembre del 2019
        * Responsable Ultima Modificación    : Yuliana Mata Corchado
        *
        /**********************************************************************************************************************/
        [HttpDelete]
        public IHttpActionResult eliminarArticulo(int id)
        {
            if (id <= 0)
                return BadRequest("Error");
            using (Models.articulosEntities db = new Models.articulosEntities())
            {
                var articulo = db.articulo.Where(s => s.idarticulo == id).FirstOrDefault();
                db.Entry(articulo).State = System.Data.Entity.EntityState.Deleted;
                db.SaveChanges();
            }
            return Ok("Eliminado");
        }


        /***********************************************************************************************************************
        * Propósito                          : Realizar peticion PUT (Para actualizar un articulo de la base de datos).
        *
        * Parámetros In                      : ID del articulo, Nombre del articulo, Descriocion,Codigo de barras, codigo del fabricante
        * Parámetros Out                     : Mensaje de confirmacion.
        *
        * Fecha de Creacion/Modificacion     : Miercoles, 6 de Noviembre del 2019
        * Responsable Ultima Modificación    : Yuliana Mata Corchado
        *
        /**********************************************************************************************************************/

        [HttpPut]
        public IHttpActionResult actualizarArticulo(Models.ArtViewModels articulomodelo)
        {
            if (!ModelState.IsValid)
                return BadRequest("Error en el modelo");
            using (Models.articulosEntities db = new Models.articulosEntities())
            {
                var exiteArticulo = db.articulo.Where(s => s.idarticulo == articulomodelo.idarticulo).FirstOrDefault<articulo>();

                if (exiteArticulo != null)
                {
                    exiteArticulo.nombre = articulomodelo.Nombre;
                    exiteArticulo.descripcion = articulomodelo.Descripcion;
                    exiteArticulo.codigoBarras = articulomodelo.CodigoBarras;
                    exiteArticulo.codigoFabricante = articulomodelo.CodigoFabricante;
                    db.SaveChanges();
                }
                else
                {
                    return NotFound();
                }
            }
            return Ok();
        }
    }
}
