﻿using System;

namespace ViewModels
{
    public class ArticuloViewModel
    {
        public int idarticulo { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string CodigoBarras { get; set; }
        public string CodigoFabricante { get; set; }
    }
}
