# API WEB

Las operaciones más impostantes que nos permitirán manipular los recursos son tres: **POST** para crear, **PUT** para editar (todos los campos) y **DELETE** para eliminar, asi mismo tambien se agrego el metodo **GET** para consultar todos los registros de la tabla.

Cuando se realiza una petición determinada, es de vital impostancia conocer si dicha operación se ha llevado a cabo de manera satisfactoria o por lo contrario se a producido algún tipo de error.

API Articulo

    POST       /api/Articulo
    PUT        /api/Articulo
    DELETE    /api/Articulo/{id}
    GET       /api/Articulo

# PROBAR CON POSTMAN

# *---------------------------- POST -------------------------------*
Cuando se realiza un **POST**, para probar abrimos postman escribimos la URL
### Ejemplo:

**http://localhost:44312/api/Articulo**

En el body selecionamos la opcion raw, y formato json, y enviamos los datos a registrar.
### Ejemplo:

```json
{
  "Nombre": "impresora",
  "Descripcion": "negra",
  "CodigoBarras": "7567567",
  "CodigoFabricante": "98098098"
}
```

# *---------------------------- PUT -------------------------------*
Cuando se realiza un **PUT**, para probar abrimos postman escribimos la URL
### Ejemplo:

**http://localhost:44312/api/Articulo**

En el body selecionamos la opcion raw, y formato json, y enviamos los datos a actualizar .
### Ejemplo:

```json
{
  "idarticulo" : "1",
  "Nombre": "Impresora DELL",
  "Descripcion": "negra",
  "CodigoBarras": "7567567",
  "CodigoFabricante": "98098098"
}
```

# *---------------------------- DELETE -------------------------------*
Cuando se realiza un **DELETE**, para probar abrimos postman escribimos la URL
### Ejemplo:

**http://localhost:44312/api/Articulo/1**

En esta opción solo agregamos a la URL el ID del articulo que deseamos eliminar de la base de datos.


# *---------------------------- GET -------------------------------*
Cuando se realiza un **GET**, para probar abrimos postman escribimos la URL
### Ejemplo:

**http://localhost:44312/api/Articulo**

En esta petición no se envia ningun dato en el body o en la URL. y como respuesta obtenemos lo siguiente:

### Ejemplo:
```json
[
    {
        "idarticulo": 2,
        "Nombre": "jabon",
        "Descripcion": null,
        "CodigoBarras": "43243242",
        "CodigoFabricante": "432243"
    },
    {
        "idarticulo": 3,
        "Nombre": "impresora",
        "Descripcion": null,
        "CodigoBarras": "3424",
        "CodigoFabricante": "43242"
    },
    {
        "idarticulo": 4,
        "Nombre": "sample string 2",
        "Descripcion": null,
        "CodigoBarras": "sample string 4",
        "CodigoFabricante": "sample string 5"
    },
    {
        "idarticulo": 5,
        "Nombre": "prueba",
        "Descripcion": null,
        "CodigoBarras": "43242",
        "CodigoFabricante": "42342"
    },
    {
        "idarticulo": 6,
        "Nombre": "sample string 2",
        "Descripcion": null,
        "CodigoBarras": "sample string 4",
        "CodigoFabricante": "sample string 5"
    },
    {
        "idarticulo": 10,
        "Nombre": "impresora epson",
        "Descripcion": null,
        "CodigoBarras": "7567567",
        "CodigoFabricante": "98098098"
    }
]
```

# BASE DE DATOS MYSQL
Para el ejercicio se utilizo conexion a **MySQL**.
se creo una base de datos llamada **articulos**
con los siguinetes campos:

|campo | Tipo |Descripción |
| --- | --- | :--- |
|**idarticulo** | int| Campo donde se genera el id del articulo |
|**nombre** |string| Nombre del articulo. |
|**descripcion**|string | descripcion del articulo. |
|**codigoBarras**|string | codigo de barras del articulo. |
|**codigoFabricante**|string | codigo del fabricante. |

## script BD

~~~sql
CREATE DATABASE  IF NOT EXISTS `articulos` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `articulos`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: articulos
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articulo`
--

DROP TABLE IF EXISTS `articulo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articulo` (
  `idarticulo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `codigoBarras` varchar(45) DEFAULT NULL,
  `codigoFabricante` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idarticulo`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-07 12:21:08
~~~~sql
